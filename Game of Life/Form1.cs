﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Media;
using System.IO;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DoubleBuffered = true;
            pictureBox1.MouseDown += new MouseEventHandler(pictureBox1_MouseDown);
            pictureBox1.MouseMove += new MouseEventHandler(pictureBox1_MouseMove);
            pictureBox1.MouseUp += new MouseEventHandler(pictureBox1_MouseUp);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            init();
            start();
        }

        // Define Variables
        private Bitmap bmpGrid;
        private Bitmap bmpCells;

        private Graphics gfxGrid;
        private Graphics gfxCells;

        private Grid grid;
        private int gridSize = 100; // default number of rows/columns in grid

        Cell[,] cellArray;
        Cell[,] cellArrayBuffered;
        private int cellsTotal;
        private int cellsAlive;
        private int cellsDead;

        private bool mousePressed = false;
        private int xMouseDown, yMouseDown;
        private int xMouseCurrent, yMouseCurrent;
        private int xGridDown, yGridDown;
        private int xGridCurrent, yGridCurrent;

        private bool gameRunning;
        private int speedStep = 20;

        private bool loading = false;
        private bool checkingRunning = false;

        private int generationNum;
        
        




        public void init() // all instances will be prepared
        {

            initGrid();
            initActiveCells();
            initBufferedCells();
                     
            // Prepare game loop
            timer1.Interval = trackBar1.Value * speedStep;
            gameRunning = false;
            generationNum = 0;
            label12.Text = generationNum.ToString();
        }

        public void initGrid() // Prepare the grid
        {
            grid = new Grid(pictureBox1.Width, pictureBox1.Height, gridSize);
            bmpGrid = new Bitmap(grid.width, grid.height);
            bmpCells = new Bitmap(grid.width, grid.height);
            gfxGrid = Graphics.FromImage(bmpGrid);
            gfxCells = Graphics.FromImage(bmpCells);
            numericUpDown1.Value = gridSize;
            cellsTotal = gridSize * gridSize;
            
            progressBar1.Maximum = cellsTotal;
            label18.Text = cellsTotal.ToString();
        }

        public void initActiveCells() // Prepare the active cells
        {
            cellsAlive = 0;
            cellsDead = 0;
            label14.Text = cellsAlive.ToString();
            label16.Text = cellsDead.ToString();
            progressBar1.Value = cellsAlive;
            cellArray = new Cell[grid.size, grid.size];

            for (int i = 0; i < cellArray.GetLength(1); i++)
            {
                for (int j = 0; j < cellArray.GetLength(0); j++)
                {
                    cellArray[i, j] = new Cell(false);
                }
            }
        }

        public void initBufferedCells() // Prepare the buffered cells
        {
            cellArrayBuffered = new Cell[grid.size, grid.size];

            for (int i = 0; i < cellArrayBuffered.GetLength(1); i++)
            {
                for (int j = 0; j < cellArrayBuffered.GetLength(0); j++)
                {
                    cellArrayBuffered[i, j] = new Cell(false);
                }
            }
        }

        public void start() // enterance to program
        {
            drawGrid();
            drawCells();
        }

        public void drawGrid() // Draw the grid
        {
            Color gridCol = new Color();
            gridCol = Color.Black;
            Pen gridPen = new Pen(gridCol);

            int x1 = 0;
            int y1 = 0;
            int x2 = 0;
            int y2 = (int)grid.scale * grid.size;

            for (int i = 1; i <= (grid.size + 1); i++) // Draw vertical grid lines
            {
                gfxGrid.DrawLine(gridPen, x1, y1, x2, y2);
                x1 = x1 + (int)grid.scale;
                x2 = x2 + (int)grid.scale;
            }

            x1 = 0;
            y1 = 0;
            x2 = (int)grid.scale * grid.size;
            y2 = 0;

            for (int i = 1; i <= (grid.size + 1); i++) // Draw horizontal grid lines
            {
                gfxGrid.DrawLine(gridPen, x1, y1, x2, y2);
                y1 = y1 + (int)grid.scale;
                y2 = y2 + (int)grid.scale;
            }
            pictureBox1.Refresh();
        }

        private void drawCells() // Draws the cells on top of the grid
        {
            gfxCells.Clear(Color.Transparent);

            SolidBrush cellBrush = new SolidBrush(Color.Red);

            // draw live cells
            for (int i = 0; i < cellArray.GetLength(1); i++)
            {
                for (int j = 0; j < cellArray.GetLength(0); j++)
                {
                    if (cellArray[i, j].isAlive == true)
                    {
                        gfxCells.FillEllipse(cellBrush, i * grid.scale, j * grid.scale, grid.scale, grid.scale);
                    }
                }
            }
            pictureBox1.Refresh();
        }

        private void pictureBox1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e) // Detects if the left mouse button has been pressed and takes the screen location with respect to the picture box
        {
            checkGameRunning();
            if (gameRunning == false)
            {
                mousePressed = true;

                // Take mouse x/y coordinates
                xMouseDown = e.X;
                yMouseDown = e.Y;

                // Convert x/y mouse down coordinates to grid referance
                xGridDown = xMouseDown / (int)grid.scale;
                yGridDown = yMouseDown / (int)grid.scale;

                // Prevent the grid location from being out of bounds
                if (xGridCurrent > (grid.size - 1))
                {
                    xGridCurrent = grid.size - 1;
                }

                if (yGridCurrent > (grid.size - 1))
                {
                    yGridCurrent = grid.size - 1;
                }

                // Prevent the grid location from being out of bounds
                if (xGridDown > (grid.size - 1))
                {
                    xGridDown = grid.size - 1;
                }

                if (yGridDown > (grid.size - 1))
                {
                    yGridDown = grid.size - 1;
                }

                // Change state of cell at that grid location
                if (cellArray[xGridDown, yGridDown].isAlive == false)
                {
                    cellArray[xGridDown, yGridDown].isAlive = true;
                }
                else
                {
                    cellArray[xGridDown, yGridDown].isAlive = false;
                }
                drawCells();
            }
        }

        private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) // Detects if the mouse has moved and takes its current position
        {
            // Show live mouse x/y coordinates
            xMouseCurrent = e.X;
            yMouseCurrent = e.Y;
            label2.Text = xMouseCurrent.ToString();
            label3.Text = yMouseCurrent.ToString();

            int xPrevGridCurrent = xGridCurrent;
            int yPrevGridCurrent = yGridCurrent;

            // convert live x/y mouse coordinates to live grid referance
            xGridCurrent = xMouseCurrent / (int)grid.scale;
            yGridCurrent = yMouseCurrent / (int)grid.scale;
            label5.Text = xGridCurrent.ToString();
            label6.Text = yGridCurrent.ToString();

            // Prevent the grid location from being out of bounds
            if (xGridCurrent > (grid.size - 1))
            {
                xGridCurrent = grid.size - 1;
            }

            if (yGridCurrent > (grid.size - 1))
            {
                yGridCurrent = grid.size - 1;
            }

            if (xGridCurrent < 0)
            {
                xGridCurrent = 0;
            }

            if (yGridCurrent < 0)
            {
                yGridCurrent = 0;
            }

            // Detect if the mouse has been dragged into a different cell and change its state
            if (mousePressed == true)
            {
                if (xGridCurrent != xPrevGridCurrent || yGridCurrent != yPrevGridCurrent)
                {
                        if (cellArray[xGridCurrent, yGridCurrent].isAlive == false)
                        {
                            cellArray[xGridCurrent, yGridCurrent].isAlive = true;
                        }
                        else
                        {
                            cellArray[xGridCurrent, yGridCurrent].isAlive = false;
                        }
                        drawCells();
                    }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) // Detects if the left mouse button has been released
        {
            mousePressed = false;
        }

        private void repaint(object sender, PaintEventArgs e) // Repaint the picturebox
        {
            e.Graphics.DrawImage(bmpGrid, 0, 0);
            e.Graphics.DrawImage(bmpCells, 0, 0);
            label14.Text = cellsAlive.ToString();
            label16.Text = cellsDead.ToString();
            label18.Text = cellsTotal.ToString();
            progressBar1.Value = cellsAlive;
        }

        private void button1_Click(object sender, EventArgs e) // Starts the game loop
        {
            gameRunning = true;
        }

        private void button2_Click(object sender, EventArgs e) // pause the game loop
        {
            gameRunning = false;
        }

        private void newGeneration() // Write the next generation into the buffered cell array
        {          
            // Clear the buffered cell array
            cellArrayBuffered = new Cell[grid.size, grid.size];

            for (int i = 0; i < cellArrayBuffered.GetLength(1); i++)
            {
                for (int j = 0; j < cellArrayBuffered.GetLength(0); j++)
                {
                    cellArrayBuffered[i, j] = new Cell(false);
                }
            }

            // Loop through each cell altering its state as necessery for the next generation
            cellsAlive = 0;
            cellsDead = 0;

            for (int i = 0; i < cellArray.GetLength(1); i++)
            {
                for (int j = 0; j < cellArray.GetLength(0); j++)
                {
                    // Is this cell populated?
                    if (cellArray[i, j].isAlive == true)
                    {
                        cellsAlive++;
                        // 1 or 0 neighbours?
                        if (numNeighbours(i, j) <= 1)
                        {
                            // Die!
                            cellArrayBuffered[i, j].isAlive = false;
                        }
                        // 4 or more neighbours?
                        else if (numNeighbours(i, j) >= 4)
                        {
                            // Die!
                            cellArrayBuffered[i, j].isAlive = false;
                        }
                        // 2 or 3 neighbours?
                        else if (numNeighbours(i, j) == 2 || numNeighbours(i, j) == 3)
                        {
                            // Survive!
                            cellArrayBuffered[i, j].isAlive = true;
                        }
                    }
                    // The cell is unpopulated
                    else
                    {
                        cellsDead++;
                        // Does it have 3 neighbours?
                        if (numNeighbours(i, j) == 3)
                        {
                            // Populate!
                            cellArrayBuffered[i, j].isAlive = true;
                        }
                    }
                }
            }

            // Copy the buffered cell array to the active cell array
            cellArray = cellArrayBuffered;
            drawCells();

            generationNum++;
            label12.Text = generationNum.ToString();
        }

        private int numNeighbours(int xLoc, int yLoc) // input a cell and outputs the number of living neighbours surrounding this cell
        {
            int result = 0;
            if (neighbourAlive(xLoc, (yLoc-1)) == true) // North cell
            {
                result++;
            }
            if (neighbourAlive((xLoc+1), (yLoc-1)) == true) // North east cell
            {
                result++;
            }
            if (neighbourAlive((xLoc+1), yLoc) == true) // West cell
            {
                result++;
            }
            if (neighbourAlive((xLoc+1), (yLoc+1)) == true) // South east cell
            {
                result++;
            }
            if (neighbourAlive(xLoc, (yLoc+1)) == true) // South cell
            {
                result++;
            }
            if (neighbourAlive((xLoc-1), (yLoc+1)) == true) // South west cell
            {
                result++;
            }
            if (neighbourAlive((xLoc-1), yLoc) == true) // West cell
            {
                result++;
            }
            if (neighbourAlive((xLoc-1), (yLoc-1)) == true) // North west cell
            {
                result++;
            }

            return result; // return the number of living neighbours
            
        }

        private bool neighbourAlive(int x, int y) // input the cell x and y location, outputs if it is alive or dead
        {
            bool result = false;

            // Check if the cell is on the boarder 
            if (x > (grid.size - 1) || x < 1 || y > (grid.size - 1) || y < 1)
            {
                result = false;
            }
            // If it's not a boarder cell check if it's alive
            else if (cellArray[x, y].isAlive == true)
            {
                result = true;
            }

            return result;
        }

        private void timer1_Tick(object sender, EventArgs e) // timer recalls the new generation method when game running is true
        {
            timer1.Interval = trackBar1.Value * speedStep;
            if (gameRunning == true)
            {
                newGeneration();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) // Save the current grid
        {
            checkGameRunning();
            if (gameRunning == false)
            {
                string gridSize = Convert.ToString(grid.size);

                SaveFileDialog save = new SaveFileDialog(); // output array to txt file
                save.FileName = "GameOfLifeState.xml";
                save.Filter = "eXtensible Markup Language | *.xml";
                if (save.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(save.OpenFile());

                    writer.WriteLine("<?xml version=\"1.0\"?>");
                    writer.WriteLine("<GOL_saved_state>");
                    writer.WriteLine("\t<Date_created>" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "</Date_created>");
                    writer.WriteLine("\t<Grid_size>" + gridSize + "</Grid_size>");
                    writer.WriteLine("\t<Cell_states>");

                    // write each state of the cell array
                    for (int i = 0; i < cellArray.GetLength(1); i++)
                    {
                        for (int j = 0; j < cellArray.GetLength(0); j++)
                        {
                            if (cellArray[i, j].isAlive == true)
                            {
                                writer.WriteLine("\t1");
                            }
                            else if (cellArray[i, j].isAlive == false)
                            {
                                writer.WriteLine("\t0");
                            }
                        }
                    }

                    writer.WriteLine("\t</Cell_states>");
                    writer.WriteLine("</GOL_saved_state>");

                    writer.Dispose();
                    writer.Close();
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e) // Load a grid
        {
            checkGameRunning();
            if (gameRunning == false)
            {
                try
                {
                    DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
                    if (result == DialogResult.OK) // Test result
                    {
                        loading = true;
                        string[] lines = System.IO.File.ReadAllLines(openFileDialog1.FileName);

                        // Load in the the txt file
                        string g = lines[3];
                        gridSize = Convert.ToInt32(Regex.Match(g, @"\d+").Value);
                        init();

                        int currentLine = 5;
                        for (int i = 0; i < gridSize; i++)
                        {
                            for (int j = 0; j < gridSize; j++)
                            {
                                if (lines[currentLine] == "\t1")
                                {
                                    cellArray[i, j].isAlive = true;
                                }
                                else if (lines[currentLine] == "\t0")
                                {
                                    cellArray[i, j].isAlive = false;
                                }
                                currentLine++;
                            }
                        }
                        //initGrid();
                        start();
                        loading = false;
                    }
                }
                catch
                {

                }
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e) // Change the grid size / scale
        {
            if (gameRunning == false)
            {
                if (loading == false)
                {
                    gridSize = (int)numericUpDown1.Value;

                    init();
                    start();
                }
            }
            else if (gameRunning == true) // this code prevents the check game running method being called twice
            {

                if (checkingRunning == false)
                {
                    checkingRunning = true;
                    checkGameRunning();
                }
                numericUpDown1.Value = gridSize;
                checkingRunning = false;
            }
        }

        private void checkGameRunning() // Tell the user to stop the game
        {
            if (gameRunning == true)
            {
                MessageBox.Show("Please stop the game first!");
            }
        }

        private void gliderCannonToolStripMenuItem_Click(object sender, EventArgs e) // Preset load state to start a glider cannon
        {
            checkGameRunning();
            if (gameRunning == false)
            {

                gridSize = 80;
                init();

                cellArray[2, 6].isAlive = true;
                cellArray[2, 7].isAlive = true;
                cellArray[3, 6].isAlive = true;
                cellArray[3, 7].isAlive = true;
                cellArray[12, 6].isAlive = true;
                cellArray[12, 7].isAlive = true;
                cellArray[12, 8].isAlive = true;
                cellArray[13, 5].isAlive = true;
                cellArray[13, 9].isAlive = true;
                cellArray[14, 4].isAlive = true;
                cellArray[14, 10].isAlive = true;
                cellArray[15, 4].isAlive = true;
                cellArray[15, 10].isAlive = true;
                cellArray[16, 7].isAlive = true;
                cellArray[17, 5].isAlive = true;
                cellArray[17, 9].isAlive = true;
                cellArray[18, 6].isAlive = true;
                cellArray[18, 7].isAlive = true;
                cellArray[18, 8].isAlive = true;
                cellArray[19, 7].isAlive = true;
                cellArray[22, 4].isAlive = true;
                cellArray[22, 5].isAlive = true;
                cellArray[22, 6].isAlive = true;
                cellArray[23, 4].isAlive = true;
                cellArray[23, 5].isAlive = true;
                cellArray[23, 6].isAlive = true;
                cellArray[24, 3].isAlive = true;
                cellArray[24, 7].isAlive = true;
                cellArray[26, 2].isAlive = true;
                cellArray[26, 3].isAlive = true;
                cellArray[26, 7].isAlive = true;
                cellArray[26, 8].isAlive = true;
                cellArray[36, 4].isAlive = true;
                cellArray[36, 5].isAlive = true;
                cellArray[37, 4].isAlive = true;
                cellArray[36, 5].isAlive = true;
                start();
            }
        }

        private void spinnerToolStripMenuItem_Click(object sender, EventArgs e) // Preset load state to start a spinner
        {
            gridSize = 5;
            init();

            cellArray[2, 1].isAlive = true;
            cellArray[2, 2].isAlive = true;
            cellArray[2, 3].isAlive = true;
            
            start();
        }





    }
}
