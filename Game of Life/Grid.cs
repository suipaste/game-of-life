﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class Grid
    {
        // fields
        public int width;
        public int height;
        public int size; // number of rows / columns in the grid;
        public float scale;

        // constructor
        public Grid(int w, int h, int s)
        {
            width = w;
            height = h;
            size = s;
            scale = width / size;
        }

        // Method - get 

    }
}
